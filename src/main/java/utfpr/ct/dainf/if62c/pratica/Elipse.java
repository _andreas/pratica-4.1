/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author a1363417
 */
public class Elipse {
    private double x, y, r, s;

    public Elipse(double r, double s) {
        this.x = r*2;
        this.y = s*2;
        this.r = r;
        this.s = s;
    }
    
    public double getArea(){
        return(Math.PI * r * s);
    }
    
    public double getPerimetro(){
        return(Math.PI * ((3.0*(r + s)) - Math.sqrt((3.0*r + s)*(r + 3.0*s))));
    }
    
    
}
